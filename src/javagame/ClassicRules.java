package javagame;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;


public class ClassicRules extends BasicGameState{
	Image Background,rules,play;
	
	
	public ClassicRules(int state) {

	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
		Background = new Image("res/background.png");
	}
	
	//Draw
	public void render(GameContainer gc, StateBasedGame sbg,Graphics g) throws SlickException {
		Background.draw(0,0);
		//g.fillOval(290, 300, 70, 30);
		//g.drawString("Play!",300 , 305);
		play = new Image("res/playbutton.png");
		rules = new Image("res/Rules.png");
		play.draw(290, 300, 81, 26);
		rules.draw(10,30,110,70);
		g.drawString("1- You start the game with 3 lives.", 10, 100);
		g.drawString("2- You lose a life with every fruit you don't slice!", 10, 120);
		g.drawString("3- Slicing the Peach will increase your score by 3!", 10, 140);
		g.drawString("4- Slicing a special fruit will increase your score by 5 or 10!", 10, 160);
		g.drawString("5- Watch out from the bomb,slice it and the game is over!", 10, 180);
		g.drawString("6- Watch out from the lives bomb,slice it and you lose a life!", 10, 200);
		g.drawString("7- Slicing a special bomb will decrease your score by 10!", 10, 220);
		g.drawString("8- As you keep slicing you will find more fruits and bombs incoming!",10,240);
		g.drawString("9- Press the (esc) button to pause", 10, 260);
	}
	
	//animation movement
	public void update(GameContainer gc, StateBasedGame sbg,int delta) throws SlickException{
		int xpos = Mouse.getX();
		int ypos = Mouse.getY();
		Input input = gc.getInput();
		//classic
		if((xpos>285 && xpos<360) && (ypos>90 && ypos<120)) {
			if(input.isMouseButtonDown(0)) {
				sbg.enterState(1);
			}
		}
	
	}
	
	public int getID() {
		return 4;
	}
}