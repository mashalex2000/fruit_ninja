package javagame;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class Game  extends StateBasedGame{
	
	public static final String gamename = "Fruit Ninja!";
	public static final int menu= 0;
	public static final int playclassic = 1;
	public static final int playarcade = 2;
	public static final int classicrules = 4;
	public static final int gameovermenu=5;
	public static final int pause=6;
	public static final int arcaderules=7;


	public Game (String gamename) {
		super(gamename); //sets the title of the game
		this.addState(new Menu(menu));
		this.addState(new PlayClassic(playclassic));
		this.addState(new PlayArcade(playarcade));
		this.addState(new ClassicRules(classicrules));
		this.addState(new ArcadeRules(arcaderules));
		this.addState(new GameOverMenu(gameovermenu));
		this.addState(new Pause(pause));
	}
	
	public void initStatesList(GameContainer gc) throws SlickException{
		this.getState(menu).init(gc, this);
		this.getState(playclassic).init(gc, this);
		this.getState(playarcade).init(gc, this);
		this.getState(classicrules).init(gc, this);
		this.getState(arcaderules).init(gc, this);
		this.getState(gameovermenu).init(gc, this);
		this.getState(pause).init(gc, this);
		this.enterState(menu);
	}
	
	public static void main(String[] args) {
		AppGameContainer appgc;
		try {
			appgc = new AppGameContainer(new Game(gamename));
			appgc.setDisplayMode(640, 420,false);
			appgc.start();
		}catch(SlickException e){
			e.printStackTrace();
		}
	}

}