package javagame;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;


public class ArcadeRules extends BasicGameState{
	Image Background,rules,play;
	
	
	public ArcadeRules(int state) {

	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
		Background = new Image("res/background.png");
	}
	
	//Draw
	public void render(GameContainer gc, StateBasedGame sbg,Graphics g) throws SlickException {
		Background.draw(0,0);
		//g.fillOval(290, 300, 70, 30);
		//g.drawString("Play!",300 , 305);
		play = new Image("res/playbutton.png");
		rules = new Image("res/Rules.png");
		play.draw(290, 320, 81, 26);
		rules.draw(10,30,110,70);
		g.drawString("1- You have only 60 seconds.", 10, 120);
		g.drawString("2- Try to beat the highest score!", 10, 140);
		g.drawString("3- Slicing the Peach score-> + 3!", 10, 160);
		g.drawString("4- Slicing the pink special fruit-> score + 5!", 10, 180);
		g.drawString("5- Slicing the red spiky special fruit-> time + 5!", 10, 200);
		g.drawString("6- Watch out from the red bomb,slice it and the game is over!", 10, 220);
		g.drawString("7- Slicing the special time bomb-> time - 10!", 10, 240);
		g.drawString("8- Each unsliced fruit fall decreases your score",10,260);
		g.drawString("9- As you keep slicing you will find more fruits and bombs incoming!",10, 280);
		g.drawString("10- Press the (esc) button to pause", 10, 300);
	}
	
	//animation movement
	public void update(GameContainer gc, StateBasedGame sbg,int delta) throws SlickException{
		int xpos = Mouse.getX();
		int ypos = Mouse.getY();
		Input input = gc.getInput();
		//classic
		if((xpos>285 && xpos<360) && (ypos>70 && ypos<100)) {
			if(input.isMouseButtonDown(0)) {
				sbg.enterState(2);
			}
		}
	
	}
	
	public int getID() {
		return 7;
	}
}
