package javagame;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name ="Highscore" )
public class BestScore {
  private int classicscore=0;
  private int arcadescore=0;
  public BestScore(){
  }
  
  public BestScore(int classicscore,int arcadescore){
      this.classicscore=classicscore;
      this.arcadescore=arcadescore;
  }
  
@XmlElement
public int getClassicscore() {
	return classicscore;
}

public void setClassicscore(int classicscore) {
	this.classicscore = classicscore;
}

@XmlElement
public int getArcadescore() {
	return arcadescore;
}

public void setArcadescore(int arcadescore) {
	this.arcadescore = arcadescore;
}

}